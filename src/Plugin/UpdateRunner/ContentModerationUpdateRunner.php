<?php

/**
 * @file
 * Contains \Drupal\bcbs_workflow\Plugin\UpdateRunner\ContentModerationUpdateRunner.
 */

namespace Drupal\bcbs_workflow\Plugin\UpdateRunner;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\scheduled_updates\ScheduledUpdateInterface;
use Drupal\scheduled_updates\ScheduledUpdateTypeInterface;
use Drupal\scheduled_updates\Plugin\UpdateRunner\LatestRevisionUpdateRunner;

/**
 * The BCBS Content Moderation Update Runner.
 *
 * @UpdateRunner(
 *   id = "bcbs_content_moderation",
 *   label = @Translation("Content Moderation"),
 *   update_types = {"embedded"},
 *   description = @Translation("Runs an update to move an entity through a moderation workflow (this is intended to be used with a content moderation state entity).")
 * )
 */
class ContentModerationUpdateRunner extends LatestRevisionUpdateRunner {

  /**
   * {@inheritdoc}
   */
  protected function loadEntitiesToUpdate($entity_ids) {
    $revision_ids = array_keys($entity_ids);
    $entity_ids = array_unique($entity_ids);
    $revisions = [];
    foreach ($entity_ids as $entity_id) {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $latest_revision */
      $latest_revision = $this->updateUtils->getLatestRevision($this->updateEntityType(), $entity_id);
      // Check the latest revision was in the revisions sent to this function.
      if (in_array($latest_revision->getRevisionId(), $revision_ids)) {
        $revisions[$entity_id] = $latest_revision;
      }
    }
    return $revisions;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['runner_advanced']['moderation_state'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Moderation state:'),
      '#description' => $this->t('Review content with current moderation state listed here'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function runUpdate(ScheduledUpdateInterface $update, $entity_ids, $queue_item) {
    return parent::runUpdate($update, $entity_ids, $queue_item);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    /** @var \Drupal\scheduled_updates\ScheduledUpdateTypeInterface $scheduled_update_type */
    $scheduled_update_type = $form_state->get('scheduled_update_type');

    if (!$scheduled_update_type->getUpdateEntityType() == 'content_moderation_state') {
      $form_state->setError(
        $form['update_entity_type'],
        $this->t('Content Moderation Running can only work with the moderation states.')
      );
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Runs an update to move an entity through a moderation workflow (this is intended to be used with a content moderation state entity).');
  }

}
