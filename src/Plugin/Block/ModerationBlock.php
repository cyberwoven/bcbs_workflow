<?php

namespace Drupal\bcbs_workflow\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\bcbs_workflow\ModerationDraftSupport;

/**
 * Provides a 'ModerationBlock' block.
 *
 * @Block(
 *  id = "moderation_block",
 *  admin_label = @Translation("Moderation block"),
 * )
 */
class ModerationBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\bcbs_workflow\ModerationDraftSupport definition.
   *
   * @var \Drupal\bcbs_workflow\ModerationDraftSupport
   */
  protected $providerModerationDrafts;

  /**
   * Constructs a new ModerationBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ModerationDraftSupport $bcbs_workflow_drafts
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->providerModerationDrafts = $bcbs_workflow_drafts;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('bcbs_workflow.drafts')
    );
  }
  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $table = $this->providerModerationDrafts->draftsAsTableArray();

    $build['moderation_block'] = [
      'message' => [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('The following providers have pending updates.'),
      ],
      'table' => $table,
      '#cache' => [
        'tags' => [
          'node_list',
        ],
        'max-age' => 60,
      ],
    ];

    return $build;
  }

}
