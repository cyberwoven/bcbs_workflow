<?php

namespace Drupal\bcbs_workflow;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\content_moderation\ModerationInformation;
use Drupal\content_moderation\Entity\ContentModerationState;
use Drupal\node\Entity\Node;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class ModerationDraftSupport.
 */
class ModerationDraftSupport {
  use StringTranslationTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;
  /**
   * Drupal\content_moderation\ModerationInformation definition.
   *
   * @var \Drupal\content_moderation\ModerationInformation
   */
  protected $contentModerationInformation;

  /**
   * Constructs a new ModerationDraftSupport object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   Entity Type Manager to build queries.
   * @param \Drupal\content_moderation\ModerationInformation $content_moderation_information
   *   Content Moderation information service to easily check status.
   */
  public function __construct(EntityTypeManager $entity_type_manager, ModerationInformation $content_moderation_information) {
    $this->entityTypeManager = $entity_type_manager;
    $this->contentModerationInformation = $content_moderation_information;
  }

  /**
   * Gets a list of all pending Drafts that currently need moderator review.
   *
   * Views should be able to do this directly, but as of 11/2017 there were
   * still too many issues in Content Moderation for that to actually work. So
   * this method provides us a work around until that is resolved. There is not
   * good reason not to switch to replace this with a view once views is up to
   * the task.
   *
   * @return array
   *   A list of nodes with pending drafts.
   */
  public function fetchPendingDrafts() {
    $pendingQuery = $this->entityTypeManager->getStorage('content_moderation_state')->getQuery()
      ->condition('moderation_state', [
        'published',
        'archived',
      ], 'NOT IN')
      ->condition('content_entity_type_id', 'node')
      ->sort('moderation_state', 'ASC');

    $pending = $pendingQuery->accessCheck(TRUE)->execute();

    $nodes = [];
    foreach ($pending as $reference) {
      $state = ContentModerationState::load($reference);
      $nid = $state->content_entity_id->value;

      $node = Node::load($nid);

      // If we have an orphaned state record we could attempt to load a deleted
      // node.
      if ($node) {
        $draft = [
          'node' => $node,
          'DefaultRevision' => $this->contentModerationInformation->getDefaultRevisionId('node', $node->id()),
          'DraftRevision' => $this->contentModerationInformation->getLatestRevisionId('node', $node->id()),
          'currentState' => $state->moderation_state->value,
        ];

        $nodes[] = $draft;
      }
    }

    return $nodes;
  }

  /**
   * Provides a table render array of pending drafts for use elsewhere.
   *
   * This is an abstraction of this process so we can use the same information
   * in the controller and block classes.
   *
   * @return array
   *   Drupal table render array.
   */
  public function draftsAsTableArray() {
    $drafts = $this->fetchPendingDrafts();

    $headers = [
      $this->t('Link'),
      $this->t('Title'),
      $this->t('Current Revision Status'),
      $this->t('Last Edited By'),
      $this->t('Submitted'),
    ];

    $table_rows = [];
    foreach ($drafts as $draft) {

      // Generate review link (using diff module).
      $review_link = Link::createFromRoute($this->t('Review'), 'diff.revisions_diff', [
        'node' => $draft['node']->id(),
        'left_revision' => $draft['DraftRevision'],
        'right_revision' => $draft['DefaultRevision'],
        'filter' => 'visual_inline',
      ]);
      $review_link = $review_link->toRenderable();

      // Generate link to current version.
      $node_link = Link::createFromRoute($draft['node']->getTitle(), 'entity.node.canonical', ['node' => $draft['node']->id()]);
      $node_link = $node_link->toRenderable();

      // Generate email link.
      $user = $draft['node']->getOwner();
      $email_link = Link::fromTextAndUrl($user->getEmail(), Url::fromUri('mailto:' . $user->getEmail()));
      $email_link = $user->getAccountName() . ": " . $email_link->toString();

      $table_rows[] = [
        'data' => [
          [
            'data' => [$review_link],
          ],
          [
            'data' => [$node_link],
          ],
          [
            'data' => [
              '#type' => 'html_tag',
              '#tag' => 'p',
              '#value' => $draft['currentState'],
            ],
          ],
          [
            'data' => [
              '#type' => 'markup',
              '#markup' => $email_link,
            ],
          ],
          [
            'data' => [
              '#type' => 'markup',
              '#markup' => date('Y-m-d H:m', $draft['node']->getChangedTime()),
            ],
          ],
        ],
      ];
    }

    $table = [
      '#type'   => 'table',
      '#title'  => $this->t('Pending Provider Updates'),
      '#header' => $headers,
      '#rows'   => $table_rows,
      '#empty'  => $this->t('There are no currently pending updates to review.'),
      '#attributes' => [
        'class' => ['provider-moderation-dashboard'],
      ],
      '#cache' => [
        'tags' => [
          'node_list',
        ],
        'max-age' => 60,
      ],
    ];

    return $table;

  }

}
