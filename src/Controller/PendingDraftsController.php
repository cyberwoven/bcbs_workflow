<?php

namespace Drupal\bcbs_workflow\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\bcbs_workflow\ModerationDraftSupport;

/**
 * Class PendingDraftsController.
 */
class PendingDraftsController extends ControllerBase {

  /**
   * Drupal\bcbs_workflow\ModerationDraftSupport definition.
   *
   * @var \Drupal\bcbs_workflow\ModerationDraftSupport
   */
  protected $providerModerationDrafts;

  /**
   * Constructs a new PendingDraftsController object.
   *
   * @param \Drupal\bcbs_workflow\ModerationDraftSupport $bcbs_workflow_drafts
   *   Service to get provider drafts easily.
   */
  public function __construct(ModerationDraftSupport $bcbs_workflow_drafts) {
    $this->moderationDrafts = $bcbs_workflow_drafts;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('bcbs_workflow.drafts')
    );
  }

  /**
   * Display a table of pending drafts.
   *
   * Views should be able to do this directly, but as of 11/2017 there were
   * still too many issues in Content Moderation for that to actually work. So
   * this method provides us a work around until that is resolved. There is not
   * good reason not to switch to replace this with a view once views is up to
   * the task.
   *
   * @return array
   *   Return render array for page with table of pending drafts.
   */
  public function drafts() {

    $table = $this->moderationDrafts->draftsAsTableArray();

    $page = [
      'message' => [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('Please review any pending provider updates'),
      ],
      'table' => $table,
      '#cache' => [
        'tags' => [
          'node_list',
        ],
        'max-age' => 60,
      ],
    ];

    return $page;
  }

}
