## BCBS Workflow Module

This module provides the workflow support for all BlueCross BlueSheild projects
managed by Cyberwoven. It defines the workflow, system emails, user roles, and
ensures that everything is setup for all existing content types when it's first
enabled.

Given that we're building something that pushes the edges of what D8 can do 
while all related modules are unstable if you are working on this module and
running into trouble look at:

* https://www.drupal.org/project/ideas/issues/2786785
* https://www.drupal.org/node/1618058
* https://drupal-media.gitbooks.io/drupal8-guide/content/modules/file_entity/intro.html
* https://www.drupal.org/project/file_entity

Likely in the spring of 2018 this will stablize a bit.


##### UPDATES:

* 02/18/2019 - "Pending Drafts" functionality has been removed. The better functionality has been added to Core. 
